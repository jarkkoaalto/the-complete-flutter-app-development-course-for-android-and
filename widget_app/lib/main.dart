import 'package:flutter/material.dart';
import './app_screens/home.dart';

void main() {
  runApp(MaterialApp(
    title: "Exploring UI widgets",
    home: Scaffold(
      appBar: AppBar(
        title: Text("Basics List View"),
      ),
      body: getListView(),
    ),
  )); // MaterialApp
}

Widget getListView() {
  var listView = ListView(
    children: <Widget>[
      ListTile(
        leading: Icon(Icons.landscape),
        title: Text("Landscape"),
        subtitle: Text("Beauiful View !"),
        trailing: Icon(Icons.wb_sunny),
        onTap: () {
          debugPrint("Lasscape tapped");
        },
      ), // ListTile
      ListTile(
        leading: Icon(Icons.laptop_chromebook),
        title: Text("Windows"),
        onTap: () {
          debugPrint("Windows tapped");
        },
      ), // ListTile
      ListTile(
        leading: Icon(Icons.phone),
        title: Text("Phone"),
        onTap: () {
          debugPrint("Phone tapped");
        },
      ), // ListTile
      Text("Yet  Another element is List"),
      Container(
        color: Colors.red,
        height: 50.0,
      )
    ], // WIdget
  ); // ListView
  return listView;
}
