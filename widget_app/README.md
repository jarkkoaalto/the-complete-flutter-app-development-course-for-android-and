# widget_app

Exploring a Commonly Flutter Application

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our 
[online documentation](https://flutter.dev/docs), which offers tutorials, 
samples, guidance on mobile development, and a full API reference.


## Section 03: Exploring Commonly used Flutter Widgets ##


## Section 04: Implementing ListView and attached Widgets ##

### ListView ###

Summary:
- Basic ListView
1. For small number of list items
2. Load all item in memory when attached to screen - Do not use it for large number of items since it is not memory efficient
3. Wrap ListView as "home" of Scaffold widget as it is scrollbar and might overflow beyond the screen


### LongList ###


