import 'package:flutter/material.dart';
import 'dart:async';

// All functions declared below runs on Main UI thread
main(){

  print('Main program: starts');

  printFileContent();
  print('Main program: ends');
}

printFileContent() async {
  String filecontent  = await downLoadFile();
  print('The content of the file is --> $filecontent');
}

// To download a file [Dummt long runnig operation}
downLoadFile(){
  Future<String> result = Future.delayed(Duration(seconds: 6), (){
    return 'My secret file content';
  }); // Future.delayed

  return result;
}