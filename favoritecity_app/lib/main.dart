import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Stateful App Example",
    home: FavoriteCity(),
  ) // MateerialApp
      );
}

class FavoriteCity extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _FavoriteCityState();
  }
}

class _FavoriteCityState extends State<FavoriteCity> {
  String nameCity = "";
  var _currencies =['Dollar', 'Euro', 'Swedish Kronor','Pound', 'Others'];
  var _currentItemSelected = "Euro";

  @override
  Widget build(BuildContext context) {
    debugPrint("Favorite City Widget is Created");

    return Scaffold(
      appBar: AppBar(
        title: Text("Stateful App Example"),
      ), // AppBar
      body: Container(
        margin: EdgeInsets.all(20.0),
        child: Column(
          children: <Widget>[
            TextField(
              onSubmitted: (String userInput) {
              // onChanged: (String userInput) {
                setState(() {
                  debugPrint(
                      "set State is called, this tells framework to redraw the FavCity widget");
                  nameCity = userInput;
                });
                nameCity = userInput;
              },
            ),

            DropdownButton<String>(
            items:_currencies.map((String dropDownStringItem){
             return DropdownMenuItem<String>(
             value: dropDownStringItem,
             child: Text(dropDownStringItem),
             );
            }).toList(), //DropdownButton
              onChanged: (String newValueSelected){
              // You code to execute, when a menu item is selected from drop sdown
                _onDropDownItemSelected(newValueSelected);
                setState((){
                  this._currentItemSelected = newValueSelected;
                });
              },
              value: _currentItemSelected,
            ),
            Padding(
                padding: EdgeInsets.all(30.0),
                child: Text(
                  "Your next city is $nameCity",
                  style: TextStyle(fontSize: 20.0),
                )) // Text
          ], // Widget
        ), // Column
      ), // Container
    ); // Scaffold
  }
  void _onDropDownItemSelected(String newValueSelected){
    setState((){
      this._currentItemSelected = newValueSelected;
    });
  }

}
