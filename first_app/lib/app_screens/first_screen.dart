import 'package:flutter/material.dart';
import 'dart:math';

class FirstScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return  Material(
      color: Colors.lightBlueAccent,
      child: Center( // Inflates the widgets
        child: Text(
          generateLuckyNumber(),
          textDirection: TextDirection.ltr,
          style: TextStyle(color: Colors.white, fontSize: 40.0),
        ),
      ),
    ); // Text
  }

  String generateLuckyNumber(){
    var random = Random();
    int luckyNumber = random.nextInt(10);

    return "Your lucky number is ${luckyNumber}";
  }
}