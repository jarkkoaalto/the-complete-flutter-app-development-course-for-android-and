# The Complete Flutter App Development Course for Android and iOS #

About this course:

A Complete Guide to Flutter Framework for building native iOS and Android apps with single code base.

## Section 01 Getting Started with Flutter ##


## Flutter installation ##

Windows Download and istallation. You have follow all steps flutter installation:

- Flutter - https://flutter.dev/docs/get-started/install/windows
- Andoroid studio - https://developer.android.com/studio
- Dart - sdhttps://dart.dev/tools/sdkk 

Download Android Studio flutter and dart plugins.

Configure -> plugins 

Restart Android studio and choose Flutter project.

# Section 2 - Summary #

We covered almost all the basic Flutter widgets
- Inroduction basics widgeets
- Row and Column
- Text
- Scaffold and Appbar
- Raised Button
- Alert Dialog
- Box Constraints
- Container
- Image
- Size, Margin and Padding
- Listview
- Floating Action Button and Scack Bar

# Section 03 - Building Interactive Flutter Application #
1. Phase
The section covers:
- What are Stateful widgets? - Differences between Stateless and Stateful widget.
- Using event handlers in app
- Dropdown Button with Dropdown Item

2. Phase
- Create a complete Stategul app: Simple Interest Calculator application
- Desing complex fluter user interface
- Apply styles and theme to your application
- Using Forms and FormTextField
- Adding validation to out form

![Screenshot](interestcalculator_app.png)

##### State #####
The state is information that can read synchronously when the widget is build and might change during the lifetime og the widget.

Class the inherit "StatefulWidget" are immmutable. But, the state is mutable.

##### Stateful vs. Stateless Widgets #####

| Stateful Widgets | Stateless Widget |
|-------------------------------------|---------------------------------------|
| When a widget changes (user interact with it) it's Stateful | No interact statee to manage or no direct user interaction, it's Stateless |
| CheckBox, RadionButton, Form, TextField | Text, RaisedButton, Icon, IconButton |
| Overrides the createState() and returns a State | Overrides the build() and returns a Widget |
| When the widget's state change, the state object call setState(), telling the framework to redraw the widget | Use when the UI depends on the information within object itself |


Using Stateful Widgets
- Create a class that extends a "StatefulWidget", that returns a Statee in "createState()"
- Create a "state"  class, with properties that may change
- Within "state" class, implement the "build()" method
- Call the setState() to make the changes. Calling setState() tells framework to redraw widget

##### Forms and TextField Validation #####

Steps to Implemet From with Validation 

- Create a Form with a GlobalKey
- Add TextFormatField with validation logic
- Use button to validate and submit the FormTextField

Section03: We covered:
- What are Stateful widgets?
- Differences between Stateless and Stateful widget.
- Using events handlers in app
- Dorpdown Butto with Dropdown Item
- Createe a Complete Stateful app: Simple Iterest Calculator application
- Desing a complex flutter user interface
- Apply styles and theme to your application
- Implementing event handlers in our application
- Using Forms and FormTextFields
- Adding validation to our form

## NoteKeeper Application ##

Section Overview

- Phase 1: Building UI and understanding foundational consepts
- Phase 2: Implementing SQLite Database

Section covers:
- Phase 1:
1. Navigating between screen in Flutter
2. Ascynronous Programming
 - future
 - Async
 - Await
 - Then
 
 Phase 2:
 - Using SQLite database with SQLite plugins
 - Singleton
 - Plugins
 1. SQFLite
 2. path_profider
 3. Intl
 
 ![Sreenshot](notekeeper_main.png)
 
 ![Screenshot](add_node_page.png)